; **注意：**
; 本脚编写是为了解决本人使用 logseq 时备份相关笔记文档，
; 可以实现当关闭 Logseq 时弹出备份提醒，跟根据配置的 
; DataPath 路径进行备份操作。
; 使用前需要变更下面 DataPath 的值为你的笔记文件目录。
; 你也可以变更 AppName 为其他应用，做适合你的备份。
; 你必须安装有 7-zip 压缩软件，并可以在命令行下执行 7z.exe

AppName := "logseq.exe"

DataPath := "D:\Temp\test_note\"

TodayStr = %A_YYYY%%A_MM%%A_DD%

Loop
{
    Process, Exist, %AppName%
    AppPID := ErrorLevel
    If (Not AppPID)
    {
        ;MsgBox, 应用程序 %AppName% 没有运行。
        Continue
    }
    Else
    {
        Loop
        {
            Sleep, 500
            Process, WaitClose, %AppName%
            AppPID := ErrorLevel
            If (AppPID)
            {
                ;MsgBox, 应用程序 %AppName% 还在运行，没有关闭；
            }
            Else
            {
                ;MsgBox, 应用程序 %AppName% 已经关闭；
                MsgBox, 36, 备份提醒, 应用程序 %AppName% 已经退出，否要对 %DataPath% 文件夹进行压缩备份？(本窗口等待5秒自动关闭), 5
                IfMsgBox, Yes
                {
                    CompBk(DataPath)
                }
                Else
                {
                    ;MsgBox, 选择了 No
                }
                Break
            }
        }
    }
}

CompBk(BkPath)
{
    TodayStr = %A_YYYY%%A_MM%%A_DD%
    ; MsgBox, 开始压缩备份 %BkPath% 
    Run, %ComSpec% /k 7z a %BkPath%\..\NoteBak%TodayStr%.7z %BkPath%\
}